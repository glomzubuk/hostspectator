﻿using System;
using BepInEx;
using BepInEx.Configuration;
using UnityEngine;
using LLBML;
using LLBML.States;

namespace HostSpectator
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class HostSpectator : BaseUnityPlugin
    {
        private ConfigEntry<KeyCode> toggleSpectatorKey;

        // Awake is called once when both the game libs and the plug-in are loaded
        void Awake()
        {
            Logger.LogInfo("Hello, world!");

            toggleSpectatorKey = this.Config.Bind<KeyCode>("Keybinds", "ToggleSpectatorKey", KeyCode.P);
        }

        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        void Update()
        {
            if (GameStates.IsInLobby() && (!NetworkApi.IsOnline || NetworkApi.IsOnline && NetworkApi.OnlineMode == OnlineMode.HOSTED) && Input.GetKeyDown(toggleSpectatorKey.Value))
            {
                GameStates.Send(new Message(Msg.SEL_SPECTATOR, 0, NetworkApi.LocalPlayerNumber, null, -1));
            }
        }
    }
}
