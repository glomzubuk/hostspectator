# ⚠️ **This mod cannot *yet* be considered tournament legal** ⚠️


Anything that touches networking should be approach carefully, and it will need extensive testing before we know for sure it doesn't impact players.
This release is to widen the testing of the mod and hopefully get rid of the potential bugs that will arise along the way.

Report any issues to : https://gitlab.com/glomzubuk/hostspectator/-/issues

Please use responsibly.


A mod that allows to toggle yourself to spectator mode, even as a host. Default hotkey is "P" and can be configured in modmenu.


After some test with AU players, it seems spec host doesn't alter the connection between players. If you find something that could hint that this assumption is false, please use the issues link above.
